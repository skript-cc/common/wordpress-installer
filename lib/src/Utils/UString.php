<?php

namespace Skript\WpInstaller\Utils;

class UString
{
    public static function kebabToCamelCase(string $string): string
    {
        return lcfirst(str_replace('-', '', ucwords($string, '-')));
    }
}
