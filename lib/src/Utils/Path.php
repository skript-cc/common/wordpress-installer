<?php

namespace Skript\WpInstaller\Utils;

const DS = DIRECTORY_SEPARATOR;

/**
 * Some utilities for dealing with paths, files and directories
 */
class Path
{
    /**
     * Creates a symlink to a specific path, at a specific location
     *
     * @param string The path to link to
     * @param string The location at which to place the link
     */
    public static function symlink(string $targetPath, string $linkPath = null): void
    {
        $cwd = getcwd();
        $absTarget = self::makeAbsolute($targetPath, $cwd);
        if ($linkPath) {
            $absLink = self::makeAbsolute($linkPath, $cwd);
            $linkName = basename($absLink);
        } else {
            $linkName = basename($absTarget);
            $absLink = $cwd.DS.$linkName;
        }

        if (file_exists($absLink)) {
            throw new \Exception("Failed to create symlink '$linkPath': file exists");
            return;
        }

        $relTargetPath = self::getRelativePath(dirname($absLink), $absTarget);

        chdir(dirname($absLink));
        symlink($relTargetPath, $linkName);
        chdir($cwd);
    }

    /**
     * Recursively creates a directory and fails silently when it already exists.
     * Similar to `mkdir -p`
     */
    public static function mkdirAndParents(string $dir, int $mode = 0755): bool
    {
        return !file_exists($dir) && mkdir($dir, $mode, true);
    }

    /**
     * Recursively removes a path and fails silently when it does not exists.
     * Similar to `rm -rf`
     */
    public static function rmrf(string $path): bool
    {
        // delete regular files and symlinks
        if (!is_dir($path) || is_link($path)) {
            return @unlink($path);
        }
        // recursively delete files and directories
        $files = array_diff(scandir($path), ['.', '..']);
        foreach ($files as $file) {
            self::rmrf($path.DS.$file);
        }
        return rmdir($path);
    }

    /**
     * Returns a relative path pointing from a source to a target
     */
    public static function getRelativePath(string $srcPath, string $targetPath): ?string
    {
        if (!self::isAbsolute($srcPath)) {
            throw new \Exception('Source path needs to be an absolute path, relative path "'.$srcPath.'" given.');
        }
        if (!self::isAbsolute($targetPath)) {
            throw new \Exception('Target path needs to be an absolute path, relative path "'.$targetPath.'" given.');
        }
        $srcParts = explode(DS, self::normalize($srcPath));
        $targetParts = explode(DS, self::normalize($targetPath));

        // root of paths do not match
        if ($srcParts[1] !== $targetParts[1]) {
            return null;
        }

        $offset = 0;
        $relPath = '';
        foreach ($srcParts as $i => $dir) {
            if (isset($targetParts[$i]) && $srcParts[$i] === $targetParts[$i]) {
                $offset++;
                continue;
            }
            $relPath .= '..'.DS;
        }

        return $relPath . implode(DS, array_slice($targetParts, $offset));
    }

    public static function getRelativePaths(string $srcPath, array $targetPaths): array
    {
        $relPaths = [];
        foreach ($targetPaths as $i => $path) {
            $relPath = self::getRelativePath($srcPath, $path);
            if ($relPath !== null) {
                $relPaths[$i] = $relPath;
            }
        }
        return $relPaths;
    }

    public static function makeAbsolute(string $path, string $absPath = '/'): string
    {
        if (strpos($path, '/') !== 0) {
            $path = rtrim($absPath, '/').'/'.$path;
        }
        return $path;
    }

    public static function normalize(string $path): string
    {
        // strip out single dot symbols
        $path = str_replace('/\./', '/', $path);
        $path = preg_replace('|^\./|', '', $path);
        $path = preg_replace('|/\.$|', '', $path);

        // resolve double dot symbols
        if (strpos($path, '..') > -1) {
            $parts = explode('/', $path);
            $newPath = [];
            // retain dots at the beginning
            $newParts = [];
            $startPos=0;
            foreach ($parts as $i => $part) {
                if ($i==$startPos && $part == '..') {
                    $startPos=$i+1;
                    continue;
                }
                $newParts[] = $part;
            }
            // resolve the rest of the part
            foreach ($newParts as $i => $part) {
                if ($part === '..') {
                    unset($newPath[count($newPath)-1]);
                    continue;
                }
                $newPath[] = $part;
            }
            $path = implode(
                '/',
                array_merge(
                    array_fill(0, $startPos, '..'), // recreate start parts
                    $newPath // append resolved path
                )
            );
        }

        return rtrim($path, '/');
    }

    public static function isRelative(string $path): bool
    {
        return !self::isAbsolute($path);
    }

    public static function isAbsolute(string $path): bool
    {
        return $path[0] === '/';
    }

    public static function join(...$args): string
    {
        return implode(DS, $args);
    }

    public static function moveDirContents(
        string $oldDir,
        string $newDir,
        array $excludes = []
    ): void {
        $paths = array_diff(scandir($oldDir), ['..', '.']);
        $paths = array_filter($paths, function ($path) use ($excludes) {
            return !in_array($path, $excludes);
        });
        foreach ($paths as $path) {
            self::rmrf(self::join($newDir, $path));
            rename(
                self::join($oldDir, $path),
                self::join($newDir, $path)
            );
        }
    }
}
