<?php

namespace Skript\WpInstaller;

use Composer\Config as ComposerConfig;
use Composer\Package\PackageInterface;
use Skript\WpInstaller\Utils\UString;
use Skript\WpInstaller\Utils\Path;

function filterFiles(array $fileArray, bool $filterVal): array
{
    return array_keys(
        array_filter(
            $fileArray,
            function (bool $keep) use ($filterVal) {
                return $keep === $filterVal;
            }
        )
    );
}

function isIndexedArray(array $value): bool
{
    return is_int(array_keys($value)[0]);
}

class Config
{
    protected $composerConfig;
    protected $packageConfig;

    protected $installDir = 'public';
    protected $configDir = 'config';
    protected $uploadsDir = 'uploads';
    protected $keep = [];
    protected $filesToKeep = [];
    protected $filesToClean = [];
    protected $wpFilesToClean = [];

    public function __construct(
        array $config,
        ComposerConfig $composerConfig,
        PackageInterface $packageConfig,
        string $overrideKey = null
    ) {
        if (isset($config[$overrideKey])) {
            $envConfig = $config[$overrideKey];
            $config = array_replace_recursive($config, $envConfig);
        }
        $this->composerConfig = $composerConfig;
        $this->packageConfig = $packageConfig;
        $this->load($config);
    }

    public function load(array $config): void
    {
        $this->installDir = $this->get($config, 'install-dir');
        $this->configDir = $this->get($config, 'config-dir');
        $this->uploadsDir = $this->get($config, 'uploads-dir');
        $this->wpFilesToClean = $config['wp-clean'] ?? [];
        $keepConfig = $config['keep'] ?? null;
        $cleanConfig = $config['clean'] ?? null;
        if ($keepConfig && !isIndexedArray($keepConfig)) {
            $this->filesToKeep = filterFiles($keepConfig ?? [], true);
            $this->filesToClean = filterFiles($keepConfig ?? [], false);
        } else {
            $this->filesToKeep = $keepConfig ?? [];
        }
        if ($cleanConfig && is_array($cleanConfig)) {
            $this->filesToClean = array_merge(
                $this->filesToClean,
                $cleanConfig
            );
        }
    }

    public function getFilesToKeep(): array
    {
        return $this->filesToKeep;
    }

    public function getFilesToClean(): array
    {
        return $this->filesToClean;
    }

    public function getWpFilesToClean(): array
    {
        return $this->wpFilesToClean;
    }

    public function getVendorDir(): string
    {
        return $this->composerConfig->get('vendor-dir');
    }

    public function getInstallDir(): string
    {
        return $this->installDir;
    }

    public function getConfigDir(): string
    {
        return $this->configDir;
    }

    public function cleanDefaultContent(): bool
    {
        return $this->cleanDefaultContent;
    }

    public function getPathsToClean(): array
    {
        return array_map(
            function (string $path): string {
                return $this->getInstallDir().'/'.$path;
            },
            $this->pathsToClean
        );
    }

    public function getUploadsDir(): string
    {
        return $this->uploadsDir;
    }

    public function getUploadsTargetPath(): string
    {
        return $this->getInstallDir().'/wp-content/uploads';
    }

    /**
     * Looks up the key in the config array. If the key does not exists it looks
     * for a property with the key in camelcase format.
     */
    private function get(array $config, string $key)
    {
        $propName = UString::kebabToCamelCase($key);
        $default = isset($this->$propName) ? $this->$propName : null;
        $value = isset($config[$key]) ? $config[$key] : $default;

        return is_array($value)
            ? array_replace_recursive($default, $value)
            : $value;
    }
}
