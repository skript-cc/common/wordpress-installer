<?php

namespace Skript\WpInstaller;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Skript\WpInstaller\Utils\Path;
use Skript\WpInstaller\Templates;

class ComposerPlugin implements PluginInterface, EventSubscriberInterface
{
    protected $config;
    protected $origInstallDir;
    protected $tmpInstallDir;

    /**
     * When this plugin is loaded, let's get this party started!
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $override = getenv('WP_INSTALLER_OVERRIDE');

        $package = $composer->getPackage();
        $extra = $package->getExtra();
        $this->config = new Config(
            $extra['wordpress-installer'] ?? [],
            $composer->getConfig(),
            $composer->getPackage(),
            $override ?? null
        );

        $this->setWordpressInstallDir($package);
        $this->setInstallerPaths($package);
    }

    /**
     * Subscribe to post install and update events to modify the wordpress
     * installation
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'post-install-cmd' => 'modifyWpInstall',
            'post-update-cmd' => 'modifyWpInstall'
        ];
    }

    /**
     * Expose WpInstallerConfig
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * Modify wordpress a bit after installation
     */
    public function modifyWpInstall(): void
    {
        $this->cleanTmpInstall();
        $this->moveTmpInstallBack();
        $this->createWpConfig();
        $this->clean();
        $this->symlinkUploads();
    }

    /**
     * Sets the wordpress intallation directories
     */
    protected function setWordpressInstallDir($package): void
    {
        $extra = $package->getExtra();
        $this->origInstallDir =
            $extra['wordpress-install-dir'] ?? $this->config->getInstallDir();
        $this->tmpInstallDir = implode(
            DIRECTORY_SEPARATOR,
            [$this->origInstallDir, '_']
        );
        unset($extra['wordpress-install-dir']);
        $package->setExtra(array_merge(
            ['wordpress-install-dir' => $this->tmpInstallDir],
            $extra
        ));
    }

    /**
     * Automatically sets installer paths based on the install dir
     */
    protected function setInstallerPaths($package): void
    {
        $extra = $package->getExtra();
        $pathConfig = [];
        if (!isset($extra['installer-paths'])) {
            $wpContentPath = Path::join($this->origInstallDir, 'wp-content');
            $pathConfig['installer-paths'] = [
                $wpContentPath.'/mu-plugins/{$name}/' => ['type:wordpress-muplugin'],
                $wpContentPath.'/plugins/{$name}/' => ['type:wordpress-plugin'],
                $wpContentPath.'/themes/{$name}/' => ['type:wordpress-theme'],
            ];
        }
        $package->setExtra(array_merge($pathConfig, $extra));
    }

    /**
     * Create wp-config file
     */
    protected function createWpConfig(): void
    {
        $absInstallDir = realpath($this->config->getInstallDir());
        $relRootPath = Path::getRelativePath($absInstallDir, getcwd());
        $vendorDir = Path::getRelativepath(
            $absInstallDir,
            $this->config->getVendorDir()
        );
        $configDir = Path::getRelativepath(
            $absInstallDir,
            Path::join(getcwd(), $this->config->getConfigDir())
        );

        // write wp-config.php
        file_put_contents(
            Path::join($this->config->getInstallDir(), 'wp-config.php'),
            Templates::wpConfig(
                Path::join($configDir, 'wp-config.php'),
                Path::join($vendorDir, 'autoload.php')
            )
        );

        // write config/wp-config.php
        $customConfigFile = Path::join(
            $this->config->getConfigDir(),
            'wp-config.php'
        );
        if (!file_exists($customConfigFile)) {
            $salts = file_get_contents('https://api.wordpress.org/secret-key/1.1/salt/');
            if (!$salts) {
                $salts = Templates::defaultSalts();
            }
            Path::mkdirAndParents(dirname($customConfigFile));
            file_put_contents(
                $customConfigFile,
                Templates::wpConfigCustom($salts)
            );
        }
    }

    /**
     * Clean paths
     */
    protected function clean(): void
    {
        // get the full path
        $pathsToClean = array_map(
            function (string $path) {
                return Path::join($this->origInstallDir, $path);
            },
            $this->config->getFilesToClean()
        );
        // remove all
        array_map(
            [__NAMESPACE__.'\Utils\Path', 'rmrf'],
            $pathsToClean
        );
    }

    /**
     * Clean paths
     */
    protected function cleanTmpInstall(): void
    {
        // get the full path
        $pathsToClean = array_map(
            function (string $path) {
                return Path::join($this->tmpInstallDir, $path);
            },
            $this->config->getWpFilesToClean()
        );
        // remove all
        array_map(
            [__NAMESPACE__.'\Utils\Path', 'rmrf'],
            $pathsToClean
        );
    }

    /**
     * Symlink the upload directory
     */
    protected function symlinkUploads(): void
    {
        $src = $this->config->getUploadsDir();
        $target = $this->config->getUploadsTargetPath();
        if (!$src) {
            return;
        }

        Path::mkdirAndParents($src, 0775);
        file_exists($target) && unlink($target);

        Path::symlink($src, $target);
    }

    /**
     * Move the temp wordpress installation back to its original location
     */
    protected function moveTmpInstallBack(): void
    {
        // move files to keep to tmp dir, so we preserve them
        array_map(
            function (string $fileToKeep) {
                $oldPath = Path::join($this->origInstallDir, $fileToKeep);
                $newPath = Path::join($this->tmpInstallDir, $fileToKeep);
                Path::rmrf($newPath);
                rename($oldPath, $newPath);
            },
            $this->config->getFilesToKeep()
        );

        // put everything back except wp-content
        Path::moveDirContents(
            $this->tmpInstallDir,
            $this->origInstallDir,
            array_merge(['wp-content'])
        );

        // put wp-content back, without overwriting plugins and themes installed
        // by composer
        array_map(
            function ($defaultWpContentPath) {
                $oldPath = Path::join($this->tmpInstallDir, $defaultWpContentPath);
                $newPath = Path::join($this->origInstallDir, $defaultWpContentPath);
                if (is_dir($oldPath)) {
                    Path::mkdirAndParents($newPath);
                    Path::moveDirContents($oldPath, $newPath);
                    return;
                }
                rename($oldPath, $newPath);
            },
            $this->getDefaultWpContentPaths()
        );

        // delete default wp content dir in temp install dir
        Path::rmrf(Path::join($this->tmpInstallDir, 'wp-content'));

        // cleanup the temp dir
        rmdir($this->tmpInstallDir);
    }

    protected function getDefaultWpContentPaths()
    {
        $defaultWpContentDir = Path::join($this->tmpInstallDir, 'wp-content');
        return array_map(
            function ($path) use ($defaultWpContentDir) {
                return Path::join('wp-content', $path);
            },
            array_diff(scandir($defaultWpContentDir), ['..', '.'])
        );
    }

    /**
     * {@inheritDoc}
     */
    public function deactivate(Composer $composer, IOInterface $io)
    {
        // intentionally left empty
    }

    /**
     * {@inheritDoc}
     */
    public function uninstall(Composer $composer, IOInterface $io)
    {
        // intentionally left empty
    }
}
