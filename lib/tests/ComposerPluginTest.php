<?php

declare(strict_types=1);

namespace Skript\WpInstaller;

use PHPUnit\Framework\TestCase;
use Composer\Plugin\Capability\CommandProvider as CommandCap;

class ComposerPluginTest extends TestCase
{
    public function testWordpressInstallDirIsSetOnPluginActivation()
    {
        $installDir = 'web';
        $tmpDir = '_';
        list($composer, $plugin) = ComposerFactory::createComposerWithActivatedPlugin([
            'extra' => [
                'wordpress-installer' => [
                    'install-dir' => $installDir
                ]
            ]
        ]);
        $extraConfig = $composer->getPackage()->getExtra();

        $this->assertEquals(
            implode(DIRECTORY_SEPARATOR, [$installDir, $tmpDir]),
            $extraConfig['wordpress-install-dir'],
            'The install directory should match the value set by wordpress-installer.install-dir'
        );
    }

    public function testWordpressInstallDirIsNotSetWhenAlreadyDefined()
    {
        $expectedInstallDir = 'web';
        $tmpDir = '_';
        list($composer, $plugin) = ComposerFactory::createComposerWithActivatedPlugin([
            'extra' => [
                'wordpress-install-dir' => $expectedInstallDir,
                'wordpress-installer' => [
                    'install-dir' => 'public'
                ]
            ]
        ]);
        $extraConfig = $composer->getPackage()->getExtra();

        $this->assertEquals(
            implode(DIRECTORY_SEPARATOR, [$expectedInstallDir, $tmpDir]),
            $extraConfig['wordpress-install-dir'],
            'The install directory should match the value set by wordpress-install-dir'
        );
    }

    public function testInstallerPathsAreSetOnPluginActivation()
    {
        list($composer, $plugin) = ComposerFactory::createComposerWithActivatedPlugin();

        $this->assertEquals(
            [
                'public/wp-content/mu-plugins/{$name}/' => ['type:wordpress-muplugin'],
                'public/wp-content/plugins/{$name}/' => ['type:wordpress-plugin'],
                'public/wp-content/themes/{$name}/' => ['type:wordpress-theme'],
            ],
            $composer->getPackage()->getExtra()['installer-paths'],
            'The installer paths should be set by default'
        );
    }

    public function testInstallerPathsAreNotSetWhenAlreadyDefined()
    {
        list($composer, $plugin) = ComposerFactory::createComposerWithActivatedPlugin([
            'extra' => [
                'installer-paths' => []
            ]
        ]);

        $this->assertEquals(
            [],
            $composer->getPackage()->getExtra()['installer-paths'],
            'The installer paths should not be set when they are already defined'
        );
    }

    public function testInstallerConfigIssetOnActivate()
    {
        list($composer, $plugin) = ComposerFactory::createComposerWithActivatedPlugin([
            'extra' => ['wordpress-installer' => ['install-dir' => 'test']]
        ]);

        $this->assertInstanceOf(Config::class, $plugin->getConfig());
        $this->assertEquals(
            'test',
            $plugin->getConfig()->getInstallDir(),
            'Configuration inside the extra.wordpress-installer section should'
            . ' be available from the Config class instance'
        );
    }

    public function testWpInstallerOverrideEnvSettingIsPassedToConfig()
    {
        putenv("WP_INSTALLER_OVERRIDE=test");
        list($composer, $plugin) = ComposerFactory::createComposerWithActivatedPlugin([
            'extra' => ['wordpress-installer' => [
                'install-dir' => 'test1',
                'test' => [
                    'install-dir' => 'test2'
                ]
            ]]
        ]);
        $this->assertEquals(
            'test2',
            $plugin->getConfig()->getInstallDir()
        );
    }
}
