<?php

declare(strict_types=1);

namespace Skript\WpInstaller;

use Composer\Composer;
use Composer\IO\NullIO;
use Composer\Factory;
use Composer\Console\Application;

class ComposerFactory
{
    public static function createComposer(array $localConfig = []): Composer
    {
        $io = new NullIO();
        $composer = (new Factory())->createComposer($io, $localConfig);
        return $composer;
    }

    public static function createComposerWithActivatedPlugin(array $composerConfig = []): array
    {
        $composer = self::createComposer($composerConfig);
        $io = new NullIO();
        $plugin = new ComposerPlugin();
        $plugin->activate($composer, $io);

        return [$composer, $plugin];
    }

    public static function getApplication()
    {
        return new Application();
    }
}
