<?php

namespace Skript\WpInstaller;

class ComposerFileMock
{
    protected $settings = [];
    protected $wpackagistRepoIsAdded = false;

    public function __construct(string $name, array $settings = [])
    {
        $this->settings['name'] = $name;
        $this->settings = array_replace_recursive($this->settings, $settings);
    }

    public function getSettings(): array
    {
        return $this->settings;
    }

    public function toJsonFile($file)
    {
        if (isset($this->settings['repositories'])) {
            $this->settings['repositories'] = array_values(
                $this->settings['repositories']
            );
        }
        return file_put_contents($file, json_encode(
            $this->settings,
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
        ));
    }

    public function addWordpressRequirement(): self
    {
        $this->settings = array_replace_recursive(
            $this->settings,
            ['require' => ['johnpbloch/wordpress' => '^5.0']]
        );
        return $this;
    }

    public function addWpInstallerRequirement(): self
    {
        $this->settings = array_replace_recursive(
            $this->settings,
            [
                'repositories' => [
                    0 => [
                        "type" => "path",
                        "url" => "../lib"
                    ]
                ],
                'require' => [
                    'skript.cc/wordpress-installer' => '*@dev'
                ],
                'config' => [
                    'allow-plugins' => [
                        'johnpbloch/wordpress-core-installer' => true,
                        'skript.cc/wordpress-installer' => true,
                        'composer/installers' => true
                    ]
                ]
            ]
        );
        return $this;
    }

    public function addTestThemeRequirement(): self
    {
        $this->settings = array_replace_recursive(
            $this->settings,
            [
                'repositories' => [
                    1 => [
                        "type" => "path",
                        "url" => "../lib/tests/Fixtures/wp-theme-test"
                    ]
                ],
                'require' => $this->getTestThemeRequirement()
            ]
        );
        return $this;
    }

    public function getTestThemeRequirement(): array
    {
        return ['skript.cc/wp-theme-test' => '*@dev'];
    }

    public function getTestThemeName(): string
    {
        return 'wp-theme-test';
    }

    public function addTestPluginRequirement(): self
    {
        $this->settings = array_replace_recursive(
            [
                'repositories' => [],
                'require' => []
            ],
            $this->settings
        );
        $this->settings['repositories'][] = [
            "type" => "path",
            "url" => "../lib/tests/Fixtures/wp-plugin-test"
        ];
        $this->settings['require'] = array_merge(
            $this->getTestPluginRequirement(),
            $this->settings['require']
        );
        return $this;
    }

    public function getTestPluginRequirement(): array
    {
        return ['skript.cc/wp-plugin-test' => '*@dev'];
    }

    public function getTestPluginName(): string
    {
        return 'wp-plugin-test';
    }

    public function addAkismetRequirement(): self
    {
        $this->settings = array_replace_recursive(
            [
                'repositories' => [],
                'require' => []
            ],
            $this->settings
        );
        $this->settings['repositories'][] = [
            "type" => "path",
            "url" => "../lib/tests/Fixtures/wp-plugin-akismet"
        ];
        $this->settings['require'] = array_merge(
            ['skript.cc/akismet' => '*@dev'],
            $this->settings['require']
        );
        return $this;
    }

    public function getAkismetPluginName(): string
    {
        return 'skript.cc/akismet';
    }

    public function addWPackagistTestPluginRequirement(): self
    {
        $this->settings = array_replace_recursive(
            [
                'repositories' => [],
                'require' => []
            ],
            $this->settings
        );
        $this->settings['repositories'][] = [
            "type" => "composer",
            "url" => "https://wpackagist.org"
        ];
        $this->settings['require']["wpackagist-plugin/hello-dolly"] = "*";
        return $this;
    }

    public function getWPackagistTestPluginName(): string
    {
        return 'hello-dolly';
    }

    public function addKeepConfiguration(array $keepConfig): self
    {
        $this->settings = array_replace_recursive(
            $this->settings,
            ['extra' => ['wordpress-installer' => ['keep' => $keepConfig]]]
        );
        return $this;
    }

    public function addCleanConfiguration(array $cleanConfig): self
    {
        $this->settings = array_replace_recursive(
            $this->settings,
            ['extra' => ['wordpress-installer' => ['clean' => $cleanConfig]]]
        );
        return $this;
    }

    public function addWpCleanConfiguration(array $cleanConfig): self
    {
        $this->settings = array_replace_recursive(
            $this->settings,
            ['extra' => ['wordpress-installer' => ['wp-clean' => $cleanConfig]]]
        );
        return $this;
    }
}
