<?php

declare(strict_types=1);

namespace Skript\WpInstaller\Features\Steps;

use Skript\WpInstaller\ComposerFileMock;
use const Skript\WpInstaller\Features\COMPOSER_INSTALL_DIR;

return [
    'Given I have created a composer.json file' => function () {
        $this->composerFile = new ComposerFileMock(
            'skript.cc/wordpress-installer-test',
            ['type' => 'project']
        );
    },

    'Given all requirements are satisfied' => function () {
        $this->composerFile
            ->addWordpressRequirement()
            ->addWpInstallerRequirement()
            ->addTestThemeRequirement()
            ->addTestPluginRequirement()
            ->addWPackagistTestPluginRequirement();
    },

    'Given I have create a .htaccess file in the installation directory' => function () {
        !file_exists(COMPOSER_INSTALL_DIR) && mkdir(COMPOSER_INSTALL_DIR);
        !file_exists(COMPOSER_INSTALL_DIR.'/public') && mkdir(COMPOSER_INSTALL_DIR.'/public');
        file_put_contents(
            COMPOSER_INSTALL_DIR.'/public/.htaccess',
            '# Test file'
        );
    },

    'Given I have specified I want to keep the .htaccess file' => function () {
        $this->composerFile->addKeepConfiguration(['.htaccess']);
    },

    'Given I have specified I want to clean readme.html' => function () {
        $this->composerFile->addCleanConfiguration(['readme.html']);
    },

    'Given I have specified I want to clean the default akismet plugin' => function () {
        $this->composerFile->addWpCleanConfiguration(['wp-content/plugins/akismet']);
    },

    'Given I have added the akismet plugin as a wpackagist dependency' => function () {
        $this->composerFile->addAkismetRequirement();
    },

    'When I run `composer install`' => function () {
        // create install dir
        !file_exists(COMPOSER_INSTALL_DIR) && mkdir(COMPOSER_INSTALL_DIR);
        $this->composerFile->toJsonFile(COMPOSER_INSTALL_DIR.'/composer.json');
        // and run composer install in it
        chdir(COMPOSER_INSTALL_DIR);
        exec('composer -q install');
    },

    'When I run `composer update`' => function () {
        // create install dir
        !file_exists(COMPOSER_INSTALL_DIR) && mkdir(COMPOSER_INSTALL_DIR);
        $this->composerFile->toJsonFile(COMPOSER_INSTALL_DIR.'/composer.json');
        // and run composer install in it
        chdir(COMPOSER_INSTALL_DIR);
        exec('composer -q update');
    },

    'Then wordpress should be installed' => function () {
        $hasWpAdminDir = file_exists(COMPOSER_INSTALL_DIR.'/public/wp-admin');
        $hasWpIncludesDir = file_exists(COMPOSER_INSTALL_DIR.'/public/wp-includes');
        $hasWpSettingsFile = file_exists(COMPOSER_INSTALL_DIR.'/public/wp-settings.php');
        $wpIsInstalled = $hasWpAdminDir && $hasWpIncludesDir && $hasWpSettingsFile;

        $this->assertTrue(
            $hasWpAdminDir,
            'Wordpress installation should have a wp-admin directory'
        );
        $this->assertTrue(
            $hasWpIncludesDir,
            'Wordpress installation should have a wp-includes directory'
        );
        $this->assertTrue(
            $hasWpSettingsFile,
            'Wordpress installation should have a wp-settings.php file'
        );
        $this->assertTrue(
            $wpIsInstalled,
            $this->getLastStepSentence()
        );
    },

    'Then I shouldn\'t loose content inside the wp-content/ directory' => function () {
        $hasTestThemeDir = file_exists(
            COMPOSER_INSTALL_DIR
            .'/public/wp-content/themes/'
            .$this->composerFile->getTestThemeName()
        );
        $hasTestPluginDir = file_exists(
            COMPOSER_INSTALL_DIR
            .'/public/wp-content/plugins/'
            .$this->composerFile->getTestPluginName()
        );
        $hasWPackagistTestPluginDir = file_exists(
            COMPOSER_INSTALL_DIR
            .'/public/wp-content/plugins/'
            .$this->composerFile->getWPackagistTestPluginName()
        );
        $uploadsPath =
            COMPOSER_INSTALL_DIR
            .'/public/wp-content/uploads';
        $hasUploadsSymlink = is_link($uploadsPath);

        $this->assertTrue(
            $hasTestThemeDir,
            'Installed themes should be present'
        );
        $this->assertTrue(
            $hasTestPluginDir,
            'Installed test plugin should be present'
        );
        $this->assertTrue(
            $hasWPackagistTestPluginDir,
            'Installed wpackagist test plugin should be present'
        );
        $this->assertTrue(
            $hasUploadsSymlink && $this->getLevelsUp(readlink($uploadsPath)) >= 2,
            'Uploads should be symlinked to a location outside of the install directory'
        );
    },

    'Then I shouldn\'t loose any configuration settings in the wp-config.php file' => function () {
        rename('config/wp-config.php', 'config/wp-config.temp.php');
        rename('public/wp-settings.php', 'public/wp-settings.temp.php');
        rename('vendor/autoload.php', 'vendor/autoload.temp.php');

        // because of the require once we can't test the inclusion on subsequent tests
        $wpConfig = file_get_contents('public/wp-config.php');
        $testWpConfig = preg_replace(
            '|require_once __DIR__."/../config/wp-config.php"|',
            'require __DIR__."/../config/wp-config.php"',
            $wpConfig
        );
        unlink('public/wp-config.php');
        file_put_contents('public/wp-config.php', $testWpConfig);

        file_put_contents('config/wp-config.php', '<?php $wpConfigIsIncluded=true;');
        file_put_contents('public/wp-settings.php', '<?php // do nothing while testing');
        file_put_contents('vendor/autoload.php', '<?php // do nothing while testing');

        require getcwd().'/public/wp-config.php';

        unlink('config/wp-config.php');
        unlink('public/wp-settings.php');
        unlink('vendor/autoload.php');

        rename('config/wp-config.temp.php', 'config/wp-config.php');
        rename('public/wp-settings.temp.php', 'public/wp-settings.php');
        rename('vendor/autoload.temp.php', 'vendor/autoload.php');

        $this->assertTrue(
            isset($wpConfigIsIncluded),
            'wp-config.php should include a config file outside of the install directory'
        );
    },

    'Then composer should correctly autoload classes from plugins and themes with an autoload configuration' => function () {
        $autoloader = require 'vendor/autoload.php';

        $hasFoundThemeClass =
            $autoloader->findFile('Skript\WpThemeTest\TestClass')
            !== false;

        $hasFoundPluginClass =
            $autoloader->findFile('Skript\WpPluginTest\TestClass')
            !== false;

        $this->assertTrue(
            $hasFoundThemeClass,
            'Classes from wordpress themes with an autoload configuration should be found by composers autoloader'
        );

        $this->assertTrue(
            $hasFoundPluginClass,
            'Classes from wordpress plugins with an autoload configuration should be found by composers autoloader'
        );
    },

    'Then the Wordpress install shouldn\'t remove the .htaccess in the installation directory' => function () {
        $hasHtAccessFile = file_exists(COMPOSER_INSTALL_DIR.'/public/.htaccess');
        $this->assertTrue(
            $hasHtAccessFile,
            'The .htaccess file should be present in the wordpress installation directory'
        );
    },

    'Then the readme.html should be gone' => function () {
        $hasReadmeFile = file_exists(COMPOSER_INSTALL_DIR.'/public/readme.html');
        $this->assertFalse(
            $hasReadmeFile,
            'The readme.html file should be removed from the wordpress installation directory'
        );
    },

    'Then the akismet plugin from wpackagist should be installed' => function () {
        $hasPluginDir = file_exists(
            COMPOSER_INSTALL_DIR
            .'/public/wp-content/plugins/akismet'
        );

        $this->assertTrue(
            $hasPluginDir,
            'The akismet plugin dir should be present in the wp-content/plugins directory'
        );

        $akismetComposerConfig = json_decode(
            file_get_contents('public/wp-content/plugins/akismet/composer.json')
        );

        $this->assertTrue(
            $akismetComposerConfig->name === $this->composerFile->getAkismetPluginName(),
            'The akismet plugin should be the plugin from wpackagist'
        );
    }
];
