<?php

declare(strict_types=1);

namespace Skript\WpInstaller\Features;

use Skript\WpInstaller\ComposerFileMock;
use Skript\WpInstaller\Utils\Path;

/**
 * @testdox Feature: Keep configuration
 */
class KeepConfigurationTest extends GherkinFeatureTest
{
    protected $composerFile;

    public static function setUpBeforeClass(): void
    {
        Path::rmrf(COMPOSER_INSTALL_DIR);
    }

    public static function tearDownAfterClass(): void
    {
        Path::rmrf(COMPOSER_INSTALL_DIR);
    }

    protected function setup(): void
    {
        Path::rmrf(COMPOSER_INSTALL_DIR);
    }

    protected function setUpSteps()
    {
        $this->defineSteps(require __DIR__.'/steps.php');
    }

    /**
     * @testdox Scenario: Installing Wordpress with Composer
     */
    public function testInstallingWordpressWithComposerRespectsFilesToKeepConfig()
    {
        $this
            ->given('I have created a composer.json file')
            ->given('all requirements are satisfied')
            ->given('I have create a .htaccess file in the installation directory')
            ->given('I have specified I want to keep the .htaccess file')
            ->when('I run `composer install`')
            ->then('the Wordpress install shouldn\'t remove the .htaccess in the installation directory');
    }

    /**
     * @testdox Scenario: Installing Wordpress with Composer
     */
    public function testInstallingWordpressWithComposerRespectsFilesToCleanConfig()
    {
        $this
            ->given('I have created a composer.json file')
            ->given('all requirements are satisfied')
            ->given('I have specified I want to clean readme.html')
            ->when('I run `composer install`')
            ->then('the readme.html should be gone');
    }

    /**
     * @testdox Scenario: Installing Wordpress with Composer
     */
    public function testInstallingWordpressWithComposerAllowsOverridingDefaultPlugins()
    {
        $this
            ->given('I have created a composer.json file')
            ->given('all requirements are satisfied')
            ->given('I have specified I want to clean the default akismet plugin')
            ->given('I have added the akismet plugin as a wpackagist dependency')
            ->when('I run `composer install`')
            ->then('the akismet plugin from wpackagist should be installed');
    }
}
