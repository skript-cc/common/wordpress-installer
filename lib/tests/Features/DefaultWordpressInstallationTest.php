<?php

declare(strict_types=1);

namespace Skript\WpInstaller\Features;

use Skript\WpInstaller\Utils\Path;

/**
 * @testdox Feature: Default Wordpress installation
 *
 * As a developer
 * I need to be able to install Wordpress without having to place it inside a subdirectory
 * (and still profit from Composer's dependency management capabilities)
 * In order to make it run in hosting environments where a default directory structure is assumed
 */
class DefaultWordpressInstallation extends GherkinFeatureTest
{
    protected $composerFile;

    public static function setUpBeforeClass(): void
    {
        Path::rmrf(COMPOSER_INSTALL_DIR);
    }

    public static function tearDownAfterClass(): void
    {
        Path::rmrf(COMPOSER_INSTALL_DIR);
    }

    protected function setup(): void
    {
        //@unlink(self::$composerInstallDir.'/composer.lock');
    }

    protected function setUpSteps()
    {
        $this->defineSteps(require __DIR__.'/steps.php');
    }

    private function getLevelsUp(string $path): int
    {
        $pathCount = array_count_values(explode('/', Path::normalize($path)));
        return $pathCount['..'] ?? 0;
    }

    /**
     * @testdox Scenario: Installing Wordpress with Composer
     */
    public function testInstallingWordpressWithComposer()
    {
        // @TODO implement And and But method for more fluent tests
        $this
            ->given('I have created a composer.json file')
            ->given('all requirements are satisfied')
            ->when('I run `composer install`')
            ->then('wordpress should be installed')
            ->then('I shouldn\'t loose content inside the wp-content/ directory')
            ->then('I shouldn\'t loose any configuration settings in the wp-config.php file')
            ->then('composer should correctly autoload classes from plugins and themes with an autoload configuration');
    }

    /**
     * @testdox Scenario: Reinstalling Wordpress with Composer
     */
    public function testReinstallingWordpressWithComposer()
    {
        $this
            ->given('I have created a composer.json file')
            ->given('all requirements are satisfied')
            ->when('I run `composer install`')
            ->then('wordpress should be installed')
            ->then('I shouldn\'t loose content inside the wp-content/ directory')
            ->then('I shouldn\'t loose any configuration settings in the wp-config.php file')
            ->then('composer should correctly autoload classes from plugins and themes with an autoload configuration');
    }

    /**
     * @testdox Scenario: Updating Wordpress with Composer
     * @depends testReinstallingWordpressWithComposer
     */
    public function testUpdatingWordpressWithComposer()
    {
        $this
            ->given('I have created a composer.json file')
            ->given('all requirements are satisfied')
            ->when('I run `composer update`')
            ->then('wordpress should be installed')
            ->then('I shouldn\'t loose content inside the wp-content/ directory')
            ->then('I shouldn\'t loose any configuration settings in the wp-config.php file')
            ->then('composer should correctly autoload classes from plugins and themes with an autoload configuration');
    }
}
