<?php

declare(strict_types=1);

namespace Skript\WpInstaller\Features;

use PHPUnit\Framework\TestCase;

abstract class GherkinFeatureTest extends TestCase
{
    protected $steps = [];
    protected $givens = [];
    protected $whens = [];
    protected $thens = [];

    public function __construct(...$args)
    {
        parent::__construct(...$args);
        // setup steps
        $setupStepsCallable = [$this, 'setUpSteps'];
        if (is_callable($setupStepsCallable)) {
            call_user_func($setupStepsCallable);
        }
    }

    protected function given(string $sentence): self
    {
        if (isset($this->givens[$sentence]) && is_callable($this->givens[$sentence])) {
            $this->steps[] = ['Given', $sentence];
            call_user_func($this->givens[$sentence]);
        }
        return $this;
    }

    protected function when(string $sentence): self
    {
        if (isset($this->whens[$sentence]) && is_callable($this->whens[$sentence])) {
            $this->steps[] = ['When', $sentence];
            call_user_func($this->whens[$sentence]);
        }
        return $this;
    }

    protected function then(string $sentence): self
    {
        if (isset($this->thens[$sentence]) && is_callable($this->thens[$sentence])) {
            $this->steps[] = ['Then', $sentence];
            call_user_func($this->thens[$sentence]);
        }
        return $this;
    }

    protected function defineStep(string $sentence, callable $stepdef): self
    {
        $lowerCaseSentence = strtolower($sentence);
        if (strpos($lowerCaseSentence, 'given') === 0) {
            $this->givens[trim(substr($sentence, 5))] = $stepdef;
        } elseif (strpos($lowerCaseSentence, 'when') === 0) {
            $this->whens[trim(substr($sentence, 4))] = $stepdef;
        } elseif (strpos($lowerCaseSentence, 'then') === 0) {
            $this->thens[trim(substr($sentence, 4))] = $stepdef;
        }
        return $this;
    }

    protected function defineSteps(array $steps): self
    {
        foreach ($steps as $sentence => $stepdef) {
            $this->defineStep($sentence, $stepdef);
        }
        return $this;
    }

    protected function getLastStepSentence(): string
    {
        return implode(' ', $this->steps[array_key_last($this->steps)]);
    }

    protected function tearDown(): void
    {
        $this->steps = [];
    }
}
