<?php

declare(strict_types=1);

namespace Skript\WpInstaller;

use PHPUnit\Framework\TestCase;
use Skript\WpInstaller\Config;
use Composer\Composer;

class ConfigTest extends TestCase
{
    protected function getComposer(array $localConfig = []): Composer
    {
        return ComposerFactory::createComposer($localConfig);
    }

    public function testDefaultValuesAreSetWhenNoConfigurationIsProvided()
    {
        $composer = $this->getComposer();
        $config = new Config([], $composer->getConfig(), $composer->getPackage());

        $this->assertEquals('public', $config->getInstallDir(), 'The default install directory should be "public"');
        $this->assertEquals('config', $config->getConfigDir(), 'The default config directory should be "config"');
        $this->assertEquals('uploads', $config->getUploadsDir(), 'The default uploads directory should be "uploads"');
        $this->assertEquals($composer->getConfig()->get('vendor-dir'), $config->getVendorDir(), 'The vendor should match the value set by composer');
    }

    public function testUploadsTargetPathPointsToWpUploadsDirectory()
    {
        $composer = $this->getComposer();
        $installDir = 'public';
        $config = new Config(
            ['install-dir' => $installDir],
            $composer->getConfig(),
            $composer->getPackage()
        );

        $this->assertEquals(
            $config->getUploadsTargetPath(),
            $installDir.'/wp-content/uploads',
            'The uploads target path should point to the directory Wordpress uses to store uploads'
        );
    }

    public function testConfigFromOverrideKeySectionOverrulesDefault()
    {
        $composer = $this->getComposer();
        $testConfig = [
            'install-dir' => 'default/install/dir/',
            'config-dir' => 'default/config/dir/',
            'uploads-dir' => 'default/uploads/dir/',
            'override' => [
                'install-dir' => 'override/install/dir/',
                'config-dir' => 'override/config/dir/',
                'uploads-dir' => 'override/uploads/dir/'
            ]
        ];
        $config = new Config(
            $testConfig,
            $composer->getConfig(),
            $composer->getPackage(),
            'override'
        );

        $this->assertEquals(
            $testConfig['override']['install-dir'],
            $config->getInstallDir(),
            'The install-dir from the override section should be used'
        );
        $this->assertEquals(
            $testConfig['override']['config-dir'],
            $config->getConfigDir(),
            'The config-dir from the override section should be used'
        );
        $this->assertEquals(
            $testConfig['override']['uploads-dir'],
            $config->getUploadsDir(),
            'The uploads-dir from the override section should be used'
        );
    }

    public function testFilesToKeepWithIndexedArray()
    {
        $composer = $this->getComposer();
        $filesToKeep = ['.htaccess', 'wp-config.php'];
        $config = new Config(
            ['keep' => $filesToKeep],
            $composer->getConfig(),
            $composer->getPackage()
        );

        $this->assertEquals(
            $filesToKeep,
            $config->getFilesToKeep(),
            'The returned config should return an unmodified array of files'
        );
    }

    /**
     * Deprecated since 0.2.0.
     */
    public function testFilesToKeepReturnsKeysWithTrueValuesFromKeepConfig()
    {
        $composer = $this->getComposer();
        $keepConfig = [
            '.htaccess' => true,
            'wp-config.php' => true
        ];
        $cleanConfig = [
            'composer.json' => false,
            'license.txt' => false,
            'readme.html' => false,
            'wp-config-sample.php' => false,
        ];
        $config = new Config(
            ['keep' => array_merge($keepConfig, $cleanConfig)],
            $composer->getConfig(),
            $composer->getPackage()
        );

        $this->assertEquals(
            array_keys($keepConfig),
            $config->getFilesToKeep(),
            'The returned config should only contain keys with true values'
        );
    }

    /**
     * Deprecated since 0.2.0.
     */
    public function testFilesToCleanWithIndexedArray()
    {
        $composer = $this->getComposer();
        $filesToClean = [
            'composer.json',
            'license.txt',
            'readme.html',
            'wp-config-sample.php',
        ];
        $config = new Config(
            ['clean' => $filesToClean],
            $composer->getConfig(),
            $composer->getPackage()
        );

        $this->assertEquals(
            $filesToClean,
            $config->getFilesToClean(),
            'The returned config should return an unmodified array of files'
        );
    }

    public function testFilesToCleanReturnsKeysWithFalseValuesFromKeepConfig()
    {
        $composer = $this->getComposer();
        $keepConfig = [
            '.htaccess' => true,
            'wp-config.php' => true
        ];
        $cleanConfig = [
            'composer.json' => false,
            'license.txt' => false,
            'readme.html' => false,
            'wp-config-sample.php' => false,
        ];
        $config = new Config(
            ['keep' => array_merge($keepConfig, $cleanConfig)],
            $composer->getConfig(),
            $composer->getPackage()
        );

        $this->assertEquals(
            array_keys($cleanConfig),
            $config->getFilesToClean(),
            'The returned config should only contain keys with false values'
        );
    }
}
