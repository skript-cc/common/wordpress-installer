<?php

declare(strict_types=1);

namespace Skript\WpInstaller;

use PHPUnit\Framework\TestCase;
use Skript\WpInstaller\Utils\Path;
use const Skript\WpInstaller\Features\COMPOSER_INSTALL_DIR;

class PathTest extends TestCase
{
    protected static $tmpDir;
    protected $cwd;

    public static function setupBeforeClass(): void
    {
        self::$tmpDir = COMPOSER_INSTALL_DIR.'-'.uniqid();
    }

    public function setup(): void
    {
        $this->cwd = getcwd();
        mkdir(self::$tmpDir);
        chdir(self::$tmpDir);
    }

    public function tearDown(): void
    {
        Path::rmrf(self::$tmpDir);
        chdir($this->cwd);
    }

    public function testSymlinkWithoutLinkPathCreatesLinkInCwd()
    {
        $targetDir = 'subdir/target_dir';
        $linkLocation = getcwd().'/target_dir';

        mkdir($targetDir, 0755, true);

        Path::symlink($targetDir);

        $this->assertTrue(is_link($linkLocation), 'target_dir should be a symlink');
        $this->assertEquals('subdir/target_dir', readlink($linkLocation), 'symlink should point to target directory');
    }

    public function testSymlinkWithLinkPathCreatesLinkInCwd()
    {
        $targetDir = 'subdir/target_dir';
        $linkLocation = getcwd().'/named_link';

        mkdir($targetDir, 0755, true);

        Path::symlink($targetDir, $linkLocation);

        $this->assertTrue(is_link($linkLocation), 'named_dir should be a symlink');
        $this->assertEquals('subdir/target_dir', readlink($linkLocation), 'symlink should point to target directory');
    }

    public function testSymlinkThrowsExceptionWhenTargetExists()
    {
        $targetDir = 'target_dir';
        $linkLocation = getcwd();

        mkdir($targetDir, 0755, true);

        $this->expectException(\Exception::class);

        Path::symlink($targetDir);
    }

    public function testGetRelativePath()
    {
        $this->assertEquals('../', Path::getRelativePath('/a/b/c', '/a/b'), 'Relative path should go one directory up');
        $this->assertEquals('c', Path::getRelativePath('/a/b', '/a/b/c'), 'Relative path should equal subdir');
        $this->assertEquals('b/c', Path::getRelativePath('/a', '/a/b/c'), 'Relative path should equal subdir');
    }

    public function testNormalizePath()
    {
        $this->assertEquals('/a/b/c', Path::normalize('/a/b/c'), 'Normal paths should stay normal');
        $this->assertEquals('b/c', Path::normalize('./b/c'), 'Single dots should be resolved');
        $this->assertEquals('/a/c', Path::normalize('/a/b/../c'), 'Double dots should be resolved');
        $this->assertEquals('../../a/b', Path::normalize('../../a/b'), 'Double dots at the beginning of a path should be retained');
        $this->assertEquals('/a/b/c', Path::normalize('/a/b/c/'), 'Trailing slashes should be stripped');
    }
}
