# Wordpress Installer

Wordpress installer for composer. Makes it possible to install Wordpress with a
default directory structure and use composer for managing plugins and themes
as dependencies.

This package provides the same functionality as John P. Blochs
[wordpress core installer], but does not require to install Wordpress in it's
own subdirectory. 

For a discussion on a similar feature request see
https://github.com/johnpbloch/wordpress-core-installer/pull/11. If installing 
wordpress in it's own subdirectory is not a problem, this package won't be of
any use.

## Installation

To use the wordpress installer add it as a requirement to composer.json besides
the johnpbloch/wordpress requirement.

```sh
composer init # only needed when starting a new project
composer require skript.cc/wordpress-installer johnpbloch/wordpress
```

Or add the requirements to your composer file directly and run `composer install`.

```json
{
    "name": "your/wordpress-installation",
    "type": "project",
    "require": {
        "skript.cc/wordpress-installer": "*@dev",
        "johnpbloch/wordpress": "^5.0",
    }
}
```

## Usage

With a default installation you end up with the following directory structure:

```
.
├── composer.json
├── composer.lock
├── config
│   └── wp-config.php
└── public
│   └── ...wordpress installation...
└── uploads
│   └── ...files uploaded in wordpress...
└── vendor
    └── ...composer packages...
```

Wordpress is installed in the `public` directory, which serves as the document
root. The uploads directory and wp-config.php file are placed outside of this
directory. That is because everything inside the `public` directory will be 
overwritten by default when `composer install` or `composer update` is run anew.
In principle that makes the `public/` directory disposable.

**TIP** It is recommended to add the public directory to your vcs ignore file.

However, there might be files which you want to keep between installs, such as 
a .htaccess file. For this purpose you can specify a keep configuration.

```json
{
    "name": "your/wordpress-installation",
    "type": "project",
    ...
    "extra": {
        "wordpress-installer": {
            "keep": [".htaccess"]
        }
    }
}
```

In addition to the keep config there's also a clean config to remove files from
the default wordpress installation you might want to remove
(such as readme.html).

```json
{
    "name": "your/wordpress-installation",
    "type": "project",
    ...
    "extra": {
        "wordpress-installer": {
            "clean": ["readme.html"]
        }
    }
}
```

### Add Wordpress plugins and themes

Since the plugins and themes directory are not placed outside of the `public`
directory you might wonder how to install any plugin or theme at all. The answer
is: through composer.

Instead of installing a plugin or theme manually or through the wordpress
interface, you add it as a composer dependency. This is especially easy when you
use the [wpackagist repository]. You only have to ensure wpackagist is listed as
a repository in your composer.json file as shown in the example below.

```json
{
    "name": "your/wordpress-installation",
    "type": "project",
    "require": {
        "skript.cc/wordpress-installer": "0.2.0",
        "johnpbloch/wordpress": "^5.0"
    },
    "repositories": [
        {
          "type": "composer",
          "url": "https://wpackagist.org"
        }
    ]
}
```

When the wpackagist repository is added you can use 
`composer require wpackagist-plugin/[plugin-name]` to install plugins, and
`composer require wpackagist-theme/[theme-name]` to install themes. You will
end up with a composer file that looks similar to this:

```json
{
    ...
    "require": {
        "skript.cc/wordpress-installer": "0.2.0",
        "johnpbloch/wordpress": "^5.0",
        "wpackagist-plugin/wordpress-seo": "*",
        "wpackagist-theme/hueman": "*"
    },
    "repositories": [
        {
          "type": "composer",
          "url": "https://wpackagist.org"
        }
    ],
    ...
}
```

## Configuration options

All configuration options should be placed inside the `extra.wordpress-installer`
field in composer.json.

* `install-dir`: Directory to install wordpress, defaults to `public/`
* `config-dir`: Directory to place wp-config.php, defaults to `config/`
* `uploads-dir`: Directory to store uploads, defaults to `uploads/`
* `keep`: Array which specifies paths to keep. Paths are relative to the
  `install-dir`.
  ```json
  {
    "extra": {
      "wordpress-installer": {
        "keep": [".htaccess"]
      }
    }
  }
  ```
* `clean`: Array which specifies paths to clean after the complete installation.
  Paths are relative to the `install-dir`.
  ```json
  {
    "extra": {
      "wordpress-installer": {
        "clean": ["wp-config.sample.php"]
      }
    }
  }
  ```
* `wp-clean`: Array which specifies paths to clean from the default wordpress
  installation. Use this to remove plugins and themes included in the default wp
  install (e.g. akismet) and keep the plugins with the same name which are
  installed through composer.
  ```json
  {
    "extra": {
      "wordpress-installer": {
        "wp-clean": ["wp-content/plugins/akismet"]
      }
    }
  }
  ```

### Edit config/wp-config.php

On installation a default wp-config.php file will be created inside the config/
directory. It's largely the same as the wp-config-sample.php file that ships
with wordpress, but it is already filled in! You are free to edit this file to 
your heart contents. Once a wp-config.php file is created it won't be overriden
on subsequent installs or updates.

When you're oke with the default settings, the only thing you need to do is to
define your database credentials. We have several suggestions how you might
approach this.

#### Store config in the environment

According to the third principle of the [twelve factor app pattern] all of the
config that is likely to vary between deploys should be stored in environment
variables. Database credentials are an excellent example of this.

In the generated config file you can see the database credentials are set by
passing in an environment variable.

```php
// ...

define('DB_NAME',       getenv('WP_DB_NAME'));
define('DB_USER',       getenv('WP_DB_USER'));
define('DB_PASSWORD',   getenv('WP_DB_PASSWORD'));
define('DB_HOST',       getenv('WP_DB_HOST') ?: 'localhost');
define('DB_CHARSET',    getenv('WP_DB_CHARSET') ?: 'utf8');
define('DB_COLLATE',    getenv('WP_DB_COLLATE') ?: '');
```

This effectively delegates the config to the environment, but that still leaves
the question how you can set environment variables for your installation at all.
Well, this heavily depends on your (development) server setup. Either use
whatever your server (e.g. apache, nginx) has to offer you, or use one of the
following options.

#### Store config in the config/.env.php file

The default wp-config.php begins with a line to include `config/.env.php`. You
can use this file to set any environment specific configuration.

Another options is to move all of the database related define statements to the
`.env.php` file and remove them from `wp-config.php`.

**TIP** Ignore `config/.env.php` in your vcs ignore file to keep your 
configuration separate from your code.

#### Use the PHP dotenv utility

The [PHP dotenv] package lets you load environment variables from a .env file.
This is especially useful if you'd like to load environment configuration in
other (bash) scripts as well. You could for example replace the .env include
with the following lines.

```php
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();
```

For more info about Dotenv, [read the docs](https://github.com/vlucas/phpdotenv).

## Contributing

Please [file an issue] when you encounter a bug or have an idea for improvement.
Merge requests are welcome.


[file an issue]: https://gitlab.com/skript-cc/common/wordpress-installer/issues/new
[wordpress core installer]: https://github.com/johnpbloch/wordpress-core-installer
[wpackagist repository]: https://wpackagist.org/
[twelve factor app pattern]: https://12factor.net/config
[ternary operator]: https://www.php.net/manual/en/language.operators.comparison.php#language.operators.comparison.ternary
[PHP dotenv]: https://github.com/vlucas/phpdotenv
