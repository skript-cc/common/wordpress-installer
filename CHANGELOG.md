# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0]
### Added
- Added wp-clean config
- Added support for PHP 8.2

### Removed
- Dropped support for PHP 7.3 and 7.4

## [0.2.2] - 2022-08-19
### Added
- Added support for PHP 8.0 and 8.1

## [0.2.1] - 2021-01-05
### Fixed
- Fixed test coverage generation

### Removed
- Dropped support for PHP 7.2

## [0.2.0] - 2021-01-05
### Added
- Added support for composer 2.0
- Added keep and clean config

### Removed
- Removed dotenv dependency
- Removed .htaccess generation

## [0.1.2] - 2020-03-24
### Fixed
- Fixed autoloading for wordpress plugins and themes (#25)

## [0.1.1] - 2019-09-01
### Fixed
- Fixed symlinking of uploads directory (#24)

## [0.1.0] - 2019-07-18
### Added
- Wordpress installation with default directory setup
- Generation of default wp-config.php file
- Generation of default .htaccess file
- Tests to add coverage for basic functionality
- Documentation with installation and usage settings


[Unreleased]: https://gitlab.com/skript-cc/common/wordpress-installer/compare/0.3.0...master
[0.3.0]: https://gitlab.com/skript-cc/common/wordpress-installer/compare/0.2.2...0.3.0
[0.2.2]: https://gitlab.com/skript-cc/common/wordpress-installer/compare/0.2.1...0.2.2
[0.2.1]: https://gitlab.com/skript-cc/common/wordpress-installer/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/skript-cc/common/wordpress-installer/compare/0.1.2...0.2.0
[0.1.2]: https://gitlab.com/skript-cc/common/wordpress-installer/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/skript-cc/common/wordpress-installer/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/skript-cc/common/wordpress-installer/-/tags/0.1.0
